package com.guilleglad.alarmsms;

import com.github.florent37.viewanimator.AnimationListener;
import com.guilleglad.alarmsms.misc.Constants;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.florent37.viewanimator.ViewAnimator;
import com.guilleglad.alarmsms.app.AppController;
import com.guilleglad.alarmsms.app.MySensor;
import com.guilleglad.alarmsms.app.MyService;
import com.guilleglad.alarmsms.net.VolleyMultipartRequest;
import com.guilleglad.alarmsms.util.AppHelper;
import com.guilleglad.alarmsms.util.CustomTarget;
import com.guilleglad.alarmsms.util.SMSSender;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.TimeUnit;


public class SensorsActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener {
    SensorsActivity _this = this;
    private Vector<MySensor> Sensors;
    private Vector<MySensor> Sensors_bin;
    private Vector<MySensor> Sensors_temp;
    private MySensor selected_sensor;
    private static final int SELECT_PICTURE = 1;
    private static final int SELECT_PICTURE_CHANGE = 2;
    private SharedPreferences settings;
    private FrameLayout fl_back;
    private ImageView bkg;
    private Button btn_a;
    private Button btn_b;
    private Button btn_m;
    private Button btn_h;
    private Button btn_e;
    private ImageButton btn_close;
    private RelativeLayout LL;
    private RelativeLayout RL;
    private int _xDelta;
    private int _yDelta;
    private Bitmap bitmap;
    private LinearLayout SMSLayout;
    private ImageView p25,p50,p75,p100;
    private TextView msg_enviando,msg_enviado;

    private String map = "";
    private String tipo_login;
    private String login = "";
    private String pass = "";
    private String sms_number = "";
    private FloatingActionButton fab;
    private Snackbar s_dialog;
    private EditText edit_id_sensor;
    private boolean fondo_cambiado = false;
    private String id_alarma = "";
    private int height;
    private int width;
    private ImageView luz;
    public static final String PREFS_NAME = "AlarmSMS_data";
    private ImageButton btn_save_sms_phone;
    private ImageButton btn_close_sms_phone;
    private EditText edit_sms_phone;
    private MyService my_service;
    private ProgressBar PBM;
    final Handler handler = new Handler();
    private TimerTask timerTaskS;
    private TimerTask timerTaskF;
    private Timer timer;
    CustomTarget CT;
    private String id_user;
    private Spinner listaFondos;
    private int check = 0;
    private String fondo_activo = "";
    private String titulo_fondo = "";
    private SMSSender mySender;
    private Button btn_activar,btn_desactivar,btn_estado,btn_activarp;
    private String activar,desactivar,estado,activarp;
    private String numero_sms_enviar,pass_sms;
    private FrameLayout layoutProgreso;
    private String sms_activar,sms_desactivar,sms_activarp,sms_estado;
    private ImageButton btn_next_map;
    private ImageButton btn_prev_map;
    private String central_activa;
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        process_extradata();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.change_user));
        mySender = new SMSSender();
        Sensors_bin = new Vector<>();
        process_extradata();
        my_service = new MyService();
        luz = (ImageView) findViewById(R.id.luz_alarma);
        layoutProgreso = (FrameLayout)findViewById(R.id.layout_progreso);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                guardar_cambios();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(fab.getContext());
                builder.setMessage(getResources().getString(R.string.msg_desea_guardar)).setPositiveButton(getResources().getString(R.string.msg_si), dialogClickListener)
                        .setNegativeButton(getResources().getString(R.string.msg_no), dialogClickListener).show();

            }
        });
        fab.setVisibility(View.INVISIBLE);
        if (tipo_login.equals("1"))
            fab.setVisibility(View.GONE);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        Intent _s = new Intent(getApplicationContext(), MyService.class);
        startService(_s);
        init_obj();
        chequea_tipo_login();
        leer_fondo(false);
        cargar_lista_fondos();

        //leerSensores(false);//SENSORES

    }

    private void cargar_lista_fondos() {
        s_dialog = Snackbar.make(fab, getResources().getString(R.string.msg_cargando_fondos), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getmaps_method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                if (!response.isEmpty() && !response.equals("0")) {
                    try {
                        List<String> list = new ArrayList<String>();
                        JSONArray jarr = new JSONArray(response);
                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject jobj = new JSONObject(jarr.getString(i));
                            String id = jobj.getString("id_fondo");
                            String nombre = jobj.getString("titulo_fondo");
                            list.add(nombre + " (" + id + ")");
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        listaFondos.setAdapter(dataAdapter);
                        String map_nro = settings.getString("map_nro", "");
                        if (!map_nro.isEmpty()) {
                            listaFondos.setSelection(Integer.parseInt(map_nro));
                        }
                        if (list.size() > 0) {
                            String item;
                            if (map_nro.isEmpty()) {
                                item = listaFondos.getItemAtPosition(0).toString();
                            } else {
                                if (Integer.parseInt(map_nro) >= list.size())
                                    map_nro = "0";
                                item = listaFondos.getItemAtPosition(Integer.parseInt(map_nro)).toString();
                            }
                            if (!item.isEmpty())
                                fondo_activo = getFondoId(item);
                        }

                        System.out.println(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    map = response;

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("map", response);
                    editor.putString("alias",titulo_fondo);
                    editor.commit();
                    resp = response;
                    CT = new CustomTarget(bkg, PBM, getApplicationContext(), "AlarmSMS", login.trim() + ".jpeg");
                    AppController.p
                            .load(resp)
                            .into(CT);
                    s_dialog.dismiss();
                    leerSensoresXfondo(fondo_activo);

                }if(response.equals("[]")){
                    Snackbar.make(listaFondos, getResources().getString(R.string.msg_no_fondos), Snackbar.LENGTH_INDEFINITE).show();
                    ((LinearLayout)findViewById(R.id.comandos_usados)).setVisibility(View.GONE);
                    RL.refreshDrawableState();
                    RL.invalidate();

                }



            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        PBM.setVisibility(View.INVISIBLE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                return params;
            }

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL1");

    }

    private void process_extradata() {
        Intent I = getIntent();
        if (I != null) {
            tipo_login = I.getStringExtra("tipo_login");
            login = I.getStringExtra("login");
            pass = I.getStringExtra("pass");
            id_user = I.getStringExtra("id_user");
            id_alarma = I.getStringExtra("ID");
            if (id_alarma != null) {
                id_alarma = id_alarma.replace("ID:", "");
                id_alarma = id_alarma.trim();
            }
        }
    }

    @Override
    protected void onResume() {
        central_activa = settings.getString("central","");
        ((TextView)findViewById(R.id.label_central)).setText(central_activa);
        numero_sms_enviar = settings.getString("numero_sms_enviar","");
        edit_sms_phone.setText(numero_sms_enviar);
        pass_sms = settings.getString("pass_sms","");
        sms_activar = settings.getString("sms_activar","");
        btn_activar.setTag(sms_activar);
        if(sms_activar.isEmpty())
            btn_activar.setEnabled(false);
        else
            btn_activar.setEnabled(true);
        sms_desactivar = settings.getString("sms_desactivar","");
        btn_desactivar.setTag(sms_desactivar);
        if(sms_desactivar.isEmpty())
            btn_desactivar.setEnabled(false);
        else
            btn_desactivar.setEnabled(true);
        sms_activarp = settings.getString("sms_activarp","");
        btn_activarp.setTag(sms_activarp);
        if(sms_activarp.isEmpty())
            btn_activarp.setEnabled(false);
        else
            btn_activarp.setEnabled(true);
        sms_estado = settings.getString("sms_estado","");
        btn_estado.setTag(sms_estado);
        if(sms_estado.isEmpty())
            btn_estado.setEnabled(false);
        else
            btn_estado.setEnabled(true);
        if(sms_activar.isEmpty() || sms_desactivar.isEmpty() || sms_activarp.isEmpty() || sms_estado.isEmpty())
            cargar_comandos();
        if(btn_activar.getTag().toString().isEmpty() || btn_desactivar.getTag().toString().isEmpty()
                || btn_activarp.getTag().toString().isEmpty() || btn_estado.getTag().toString().isEmpty())
            cargar_comandos();
        cargar_comandos();
        super.onResume();
    }

    private void leerFondoTimer() {
        timer = new Timer();
        carga_fondo_local();
        timer.schedule(timerTaskF, 1000);
    }

    private void leerSensoresTimer() {
        timer = new Timer();
        carga_sensores_local();
        timer.schedule(timerTaskS, 1000);
    }

    private void chequea_tipo_login() {
        if (tipo_login.equals("1")) {
            fab.setVisibility(View.GONE);
        } else {
            luz.setVisibility(View.GONE);
        }
    }

    private void guardar_cambios() {
        s_dialog = Snackbar.make(bkg, getResources().getString(R.string.msg_enviando), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();
        guardar_fondo();
        guardar_sensores();

    }


    private void init_obj() {
        Sensors = new Vector<MySensor>();
        Sensors_temp = new Vector<MySensor>();
        settings = getSharedPreferences(PREFS_NAME, 0);
        edit_id_sensor = (EditText) findViewById(R.id.edit_id_sensor);
        edit_sms_phone = (EditText) findViewById(R.id.edit_sms_number);
        edit_sms_phone.setText(settings.getString("sms_number", ""));
        id_user = settings.getString("id_user", "");


        listaFondos = (Spinner) findViewById(R.id.spinnerFondos);
        listaFondos.setOnItemSelectedListener(this);
        bkg = (ImageView) findViewById(R.id.my_bkg);
        btn_a = (Button) findViewById(R.id.btn_apertura);
        btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton(v);
                addSensor("a",false);
            }
        });
        btn_b = (Button) findViewById(R.id.btn_barrera);
        btn_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton(v);
                addSensor("b",false);
            }
        });
        btn_m = (Button) findViewById(R.id.btn_movimiento);
        btn_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton(v);
                addSensor("m",false);
            }
        });
        btn_h = (Button)findViewById(R.id.btn_humo);
        btn_h.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton(v);
                addSensor("h",false);
            }
        });
        btn_e = (Button)findViewById(R.id.btn_exterior);
        btn_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton(v);
                addSensor("e",false);
            }
        });
        btn_close = (ImageButton) findViewById(R.id.btn_cerrar_menu);
        btn_save_sms_phone = (ImageButton) findViewById(R.id.btn_save_sms_phone);
        btn_close_sms_phone = (ImageButton) findViewById(R.id.btn_close_sms_phone);
        PBM = (ProgressBar) findViewById(R.id.progressBarMap);

        LL = (RelativeLayout) findViewById(R.id.floating_menu);
        SMSLayout = (LinearLayout) findViewById(R.id.floating_sms_number);

        //EVENTOS
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_id_sensor.setText("");
                LL.setVisibility(View.GONE);
            }
        });
        btn_save_sms_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("sms_number", edit_sms_phone.getText().toString());
                editor.commit();
                btn_close_sms_phone.performClick();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_sms_phone_guardado), Toast.LENGTH_LONG).show();
            }
        });
        btn_close_sms_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SMSLayout.setVisibility(View.GONE);
            }
        });
        RL = (RelativeLayout) findViewById(R.id.fl_back);

        View.OnClickListener myClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ViewAnimator
                        .animate(v)
                        .tada()
                        .duration(500)
                        .accelerate()
                        .start().onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if(numero_sms_enviar.isEmpty()) {
                            Snackbar.make(btn_activar, getResources().getString(R.string.error_number_empty2), Snackbar.LENGTH_LONG).show();
                        }else{
                            if(pass_sms.isEmpty()) {
                                Snackbar.make(btn_activar, getResources().getString(R.string.error_empty_pass2), Snackbar.LENGTH_LONG).show();
                            }else if(v.getTag().toString().isEmpty()){
                                Snackbar.make(btn_activar, getResources().getString(R.string.error_empty_tag), Snackbar.LENGTH_LONG).show();
                            }
                            else{
                                animarProgreso();
                                String fondo="";
                                if(listaFondos.getSelectedItem() == null)
                                {
                                    fondo = settings.getString("alias","");
                                }else{
                                    fondo = listaFondos.getSelectedItem().toString();
                                    fondo = fondo.substring(0,fondo.indexOf("(")-1);
                                }
                                if(fondo.isEmpty())
                                {
                                    Snackbar.make(bkg,getResources().getString(R.string.err_enviando_sms), Snackbar.LENGTH_LONG).show();
                                    return;
                                }
                                mySender.enviarSMS((String) v.getTag(),
                                        numero_sms_enviar,
                                        pass_sms,
                                        fondo,
                                        getApplicationContext());
                            }
                        }
                    }
                });
            }
        };
        btn_activar = (Button)findViewById(R.id.btn_activar);
        btn_activar.setOnClickListener(myClickListener);
        btn_desactivar = (Button)findViewById(R.id.btn_desactivar);
        btn_desactivar.setOnClickListener(myClickListener);
        btn_estado = (Button)findViewById(R.id.btn_estado);
        btn_estado.setOnClickListener(myClickListener);
        btn_activarp=(Button)findViewById(R.id.btn_activarp);
        btn_activarp.setOnClickListener(myClickListener);
        p25 = (ImageView)findViewById(R.id.p25);
        p50 = (ImageView)findViewById(R.id.p50);
        p75 = (ImageView)findViewById(R.id.p75);
        p100 = (ImageView)findViewById(R.id.p100);
        msg_enviando = (TextView)findViewById(R.id.msg_enviando);
        msg_enviado = (TextView)findViewById(R.id.msg_enviado);

        btn_next_map = (ImageButton)findViewById(R.id.btn_next_map);
        btn_prev_map = (ImageButton)findViewById(R.id.btn_prev_map);
        btn_next_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator.animate(btn_next_map).rubber().duration(250).onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if(listaFondos.getSelectedItemPosition()+1 <= listaFondos.getCount()-1)
                            listaFondos.setSelection(listaFondos.getSelectedItemPosition()+1);
                    }
                }).start();
            }
        });
        btn_prev_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator.animate(btn_prev_map).rubber().duration(250).onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        if(listaFondos.getSelectedItemPosition()-1 >= 0)
                            listaFondos.setSelection(listaFondos.getSelectedItemPosition()-1);
                    }
                }).start();
            }
        });
    }

    private void animarProgreso() {
        long intervalos = 1200;
        long in = 1000;
        long out = 400;
        ViewAnimator
                .animate(layoutProgreso)
                .alpha(0,1)
                .duration(in)
                .accelerate()
                .thenAnimate(p25)
                .alpha(0,1)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p50)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p25)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p75)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p50)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p100)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p75)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p100)
                .alpha(1,0)
                .duration(intervalos)
                .andAnimate(msg_enviado)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(msg_enviando)
                .descelerate()
                .thenAnimate(p100)
                .alpha(1,0)
                .duration(out)
                .andAnimate(layoutProgreso)
                .alpha(1,0)
                .duration(out)
                .andAnimate(msg_enviado)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .start();

    }

    private void cargar_comandos() {
        StringRequest comandosRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getComandos, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                System.out.println(response);
                if (!response.isEmpty() && !response.equals("0")) {
                    try {

                        JSONArray jarr = new JSONArray(response);
                        JSONObject jobj = new JSONObject(jarr.getString(0));
                        SharedPreferences.Editor editor = settings.edit();
                        activar = jobj.getString("sms_activar_central");
                        if(activar.isEmpty() || tipo_login.equals("2")) {
                            btn_activar.setEnabled(false);
                            btn_activar.setBackgroundColor(Color.GRAY);
                            btn_activar.setTag("");
                            editor.putString("sms_activar","");
                        }
                        else {
                            btn_activar.setTag(activar);
                            editor.putString("sms_activar",activar);
                        }
                        activarp = jobj.getString("sms_activarparcial_central");
                        if(activarp.isEmpty() || tipo_login.equals("2")){
                            btn_activarp.setEnabled(false);
                            btn_activarp.setBackgroundColor(Color.GRAY);
                            btn_activarp.setTag("");
                            editor.putString("sms_activarp","");
                        }
                        else {
                            btn_activarp.setTag(activarp);
                            editor.putString("sms_activarp",activarp);
                        }
                        desactivar = jobj.getString("sms_desactivar_central");
                        if(desactivar.isEmpty() || tipo_login.equals("2")) {
                            btn_desactivar.setEnabled(false);
                            btn_desactivar.setBackgroundColor(Color.GRAY);
                            btn_desactivar.setTag("");
                            editor.putString("sms_desactivar","");
                        }
                        else {
                            btn_desactivar.setTag(desactivar);
                            editor.putString("sms_desactivar",desactivar);
                        }
                        estado = jobj.getString("sms_estado_central");
                        if(estado.isEmpty() || tipo_login.equals("2")) {
                            btn_estado.setEnabled(false);
                            btn_estado.setBackgroundColor(Color.GRAY);
                            btn_estado.setTag("");
                            editor.putString("sms_estado","");
                        }
                        else {
                            btn_estado.setTag(estado);
                            editor.putString("sms_estado",estado);
                        }
                        editor.commit();
                        //Toast.makeText(SensorsActivity.this,central_activa,Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//                    s_dialog.dismiss();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(listaFondos, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("central",central_activa);
                return params;
            }

        };
        comandosRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(comandosRequest, "GET_COMANDOS");

    }

    private void animarBoton(View v) {
        ViewAnimator
                .animate(v)
                .tada()
                .duration(500)
                .start();
    }

    private void leer_fondo(boolean refresh) {
        if (fondo_cambiado && refresh == false)
            return;
        PBM.setVisibility(View.VISIBLE);
        s_dialog = Snackbar.make(fab, getResources().getString(R.string.msg_cargando_fondo), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();
        central_activa = settings.getString("central","");
        if (settings.getString("map", "").equals("") || refresh) {
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getmap_method, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
//                Log.d("Response", response);
                    final String resp;
                    if (!response.isEmpty() && !response.equals("0")) {
                        try {
                            JSONObject json_obj = new JSONObject(response);
                            map = json_obj.getString("map");
                            central_activa = json_obj.getString("central");
                            //Toast.makeText(SensorsActivity.this, central_activa, Toast.LENGTH_SHORT).show();
                            ((TextView)findViewById(R.id.label_central)).setText(central_activa);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("map", map);
                            editor.putString("central",central_activa);
                            editor.commit();
                            CT = new CustomTarget(bkg, PBM, getApplicationContext(), "AlarmSMS", login.trim() + ".jpeg");
                            AppController.p
                                    .load(map)
                                    .into(CT);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    PBM.setVisibility(View.INVISIBLE);
                    s_dialog.dismiss();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                            s_dialog.dismiss();
                            Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                            PBM.setVisibility(View.INVISIBLE);
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("login", login);
                    params.put("pass", pass);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL2");
        } else {
            leerFondoTimer();

        }
    }

    private void carga_fondo_local() {
        timerTaskF = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        ContextWrapper cw = new ContextWrapper(getApplicationContext());
                        File directory = cw.getDir("AlarmSMS", Context.MODE_PRIVATE);
                        File myImageFile = new File(directory, login.trim() + ".jpeg");
                        AppController.p
                                .with(bkg.getContext())
                                .load(myImageFile)
                                .noPlaceholder()
                                .centerCrop()
                                .fit()
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(bkg, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        PBM.setVisibility(View.INVISIBLE);
                                        s_dialog.dismiss();
                                    }

                                    @Override
                                    public void onError() {
                                        PBM.setVisibility(View.INVISIBLE);
                                        s_dialog.dismiss();
                                    }
                                });
                    }
                });
            }

            ;
        };

    }

    private MySensor addSensor(String tipo,boolean cargando) {

        edit_id_sensor = (EditText) findViewById(R.id.edit_id_sensor);
        if (edit_id_sensor.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_empty_id_sensor), Toast.LENGTH_LONG).show();
//            Snackbar.make(edit_id_sensor,getResources().getString(R.string.error_empty_id_sensor),Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(!cargando)
            edit_id_sensor.setText(fondo_activo+edit_id_sensor.getText().toString());
        for (MySensor ms : Sensors) {
            if (edit_id_sensor.getText().toString().equals(String.valueOf(ms.getID()))) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_id_existe), Toast.LENGTH_LONG).show();
                //Snackbar.make(fab,getResources().getString(R.string.error_id_existe),Snackbar.LENGTH_LONG).show();
                edit_id_sensor.selectAll();
                return null;
            }
        }

        final MySensor new_sensor = new MySensor(this);
        switch (tipo) {
            case "a":
                new_sensor.setImageResource(R.mipmap.sensor_apertura);
                break;
            case "b":
                new_sensor.setImageResource(R.mipmap.sensor_barrera);
                break;
            case "m":
                new_sensor.setImageResource(R.mipmap.sensor_movimiento);
                break;
            case "h":
                new_sensor.setImageResource(R.mipmap.ic_launcher);
                break;
            case "e":
                new_sensor.setImageResource(R.mipmap.ic_launcher);
                break;
        }

        new_sensor.setScaleType(ImageView.ScaleType.FIT_XY);

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        //int titleBarHeight= contentViewTop - statusBarHeight;

        new_sensor.setLayoutParams(new ViewGroup.LayoutParams(statusBarHeight, statusBarHeight));

        registerForContextMenu(new_sensor);

        new_sensor.setOnTouchListener(new View.OnTouchListener() {
            boolean moved = false;

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                final int Y = (int) event.getRawY();
                if (tipo_login.equals("1")) {
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            _xDelta = X - lParams.leftMargin;
                            _yDelta = Y - lParams.topMargin;
                            moved = false;

                            break;
                        case MotionEvent.ACTION_UP:
                            selected_sensor = new_sensor;
                            int pos = Sensors.indexOf(selected_sensor);
//                            Log.i("ID Selected", String.valueOf((Sensors.elementAt(pos)).getID()));
                            if (!moved) {
                                _this.openContextMenu(new_sensor);
                                break;
                            }
                        case MotionEvent.ACTION_POINTER_DOWN:
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            layoutParams.leftMargin = X - _xDelta;
                            layoutParams.topMargin = Y - _yDelta;
                            layoutParams.rightMargin = -250;
                            layoutParams.bottomMargin = -250;
                            view.setLayoutParams(layoutParams);
                            new_sensor.setPosX(layoutParams.leftMargin);
                            new_sensor.setPosY(layoutParams.topMargin);
//                            Log.i("x:", String.valueOf(view.getLeft()));
//                            Log.i("y:", String.valueOf(view.getTop()));
//                        Log.i("posX: ", String.valueOf(new_sensor.getPosX()));
//                        Log.i("posY: ", String.valueOf(new_sensor.getPosY()));
                            moved = true;
                            break;

                    }
                } else {
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            selected_sensor = new_sensor;
                            int pos = Sensors.indexOf(selected_sensor);
//                            Log.i("ID Selected", "Sensor Nº "+String.valueOf((Sensors.elementAt(pos)).getID()));
                            Toast.makeText(getApplicationContext(), "Sensor Nº " + String.valueOf((Sensors.elementAt(pos)).getID()), Toast.LENGTH_LONG).show();
                            ViewAnimator
                                    .animate(view)
                                    .pulse()
                                    .duration(1000)
                                    .repeatCount(1)
                                    .start();
                            break;

                    }
                }

                return true;
            }
        });
        new_sensor.setID(Integer.parseInt(edit_id_sensor.getText().toString()));
        new_sensor.setTipo(tipo);
        RL.addView(new_sensor, 1);
        new_sensor.postInvalidate();
        Sensors.add(new_sensor);
        edit_id_sensor.setText("");
//        Toast.makeText(getApplicationContext(),getString(R.string.msn_sensor_creado),Toast.LENGTH_LONG).show();
        if (id_alarma != null && !id_alarma.isEmpty()) {
            Snackbar.make(edit_id_sensor, getResources().getString(R.string.msg_alarma) + " " + id_alarma, Snackbar.LENGTH_LONG).show();
            if (id_alarma.equals(String.valueOf(new_sensor.getID()))) {
                luz.setVisibility(View.VISIBLE);
                ViewAnimator
                        .animate(new_sensor)
                        .duration(2000)
                        .rubber()
                        .dp().pulse()
                        .andAnimate(luz)
                        .alpha(0, 1)
                        .duration(500)
                        .repeatCount(-1)
                        .start();

            }
        }
        return new_sensor;
//        Snackbar.make(edit_id_sensor,getResources().getString(R.string.msn_sensor_creado),Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        int ID = ((MySensor) v).getID();
        menu.setHeaderTitle("Sensor " + String.valueOf(ID));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.opt_eliminar_sensor:
                RL.removeViewInLayout(selected_sensor);
                Sensors_bin.add(selected_sensor);
                Sensors.remove(selected_sensor);
                RL.invalidate();
                break;
        }
        return true;
    }

    private void eliminar_sensores() {
        {
            for (final MySensor ms : Sensors_bin) {
                StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.ELIMINA_SENSOR_FONDO, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
//                        Log.d("Eliminando Sensor: ", response);
                        if (response.equals("1")) {
//                            Log.d("Eliminando Sensor:","Sensor "+ms.getID()+"Eliminado");
                        } else {
//                            Log.d("Eliminando Sensor:","Sensor "+ms.getID()+"No pudo ser Eliminado");
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
//                                Log.d("Error.Response", "VOLLEYERROR: " + error.getStackTrace());
                                s_dialog.dismiss();
                                Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("login", login);
                        params.put("pass", pass);
                        params.put("sensor_id", String.valueOf(ms.getID()));
                        params.put("fondo", fondo_activo);
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/x-www-form-urlencoded");
                        return headers;
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(postRequest, "GUARDA_SENSOR");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (tipo_login.equals("2"))
            inflater.inflate(R.menu.menu, menu);
        else
            inflater.inflate(R.menu.menu1, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_activar_x:
                btn_activar.callOnClick();
                break;
            case R.id.btn_activarp_x:
                btn_activarp.callOnClick();
                break;
            case R.id.btn_desactivar_x:
                btn_desactivar.callOnClick();
                break;
            case R.id.btn_estado_x:
                btn_estado.callOnClick();
                break;
            case R.id.opt_logout:
                logout();
                break;
            case R.id.opt_delete:
                if(selected_sensor != null)
                    _this.openContextMenu(selected_sensor);
                else
                    Snackbar.make(listaFondos,getResources().getString(R.string.msg_no_sensor_selected), Snackbar.LENGTH_LONG).show();
                break;
            case R.id.opt_new_sensor:
                if (listaFondos.getChildCount() > 0) {
                    LL.setAlpha(0);
                    LL.setVisibility(View.VISIBLE);
                    ViewAnimator.animate(LL).alpha(0, 1).duration(500).start();
                }
                else
                    Snackbar.make(listaFondos, getResources().getString(R.string.msg_no_listafondos), Snackbar.LENGTH_LONG).show();
                break;
            case R.id.opt_save:
                fab.performClick();
                break;
            case R.id.opt_sms_number:
                SMSLayout.setAlpha(0);
                SMSLayout.setVisibility(View.VISIBLE);
                ViewAnimator.animate(SMSLayout).alpha(0,1).duration(500).start();
                break;
            case R.id.opt_comandos:
                if(listaFondos.getChildCount()==0) {
                    Snackbar.make(listaFondos, getResources().getString(R.string.msg_no_fondos), Snackbar.LENGTH_LONG).show();
                }
                else {
                    Intent _IComandos = new Intent(this, EnviosActivity.class);
                    _IComandos.putExtra("login", login);
                    _IComandos.putExtra("pass", pass);
                    _IComandos.putExtra("central",central_activa);
                    startActivity(_IComandos);
                }
                break;
            case R.id.opt_refresh_background:
                leer_fondo(true);
//                leerSensores(true);
                leerSensoresXfondo(fondo_activo);
                break;
            case R.id.opt_set_title:
                setTitulo();
                break;
            case R.id.opt_set_background:
                if (listaFondos.getChildCount() > 0)
                    pickImage(SELECT_PICTURE);
                else
                    agregarMapa();
                break;
            case R.id.opt_del_background:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                borraNivelSensores();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(fab.getContext());
                builder.setMessage(getResources().getString(R.string.msg_desea_eliminar_todo)).setPositiveButton(getResources().getString(R.string.msg_si), dialogClickListener)
                        .setNegativeButton(getResources().getString(R.string.msg_no), dialogClickListener).show();

                break;
            case R.id.opt_add_background:
                agregarMapa();
                break;
            case android.R.id.home:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void logout() {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("logged_in", Integer.parseInt("0"));
        editor.commit();
        Intent I = new Intent(getApplicationContext(), MainActivity.class);

        startActivity(I);
        finish();
    }
    private void setTitulo() {
        if(fondo_activo.isEmpty()){
            Snackbar.make(bkg,getResources().getString(R.string.err_fondo_activo),Snackbar.LENGTH_LONG).show();
            return;
        }
        final EditText editTitulo = new EditText(this);
        editTitulo.setSingleLine();
        editTitulo.setHint("Titulo del Mapa");
        editTitulo.setText(titulo_fondo);
        AlertDialog.Builder abuilder = new AlertDialog.Builder(this);
        abuilder.setTitle("Modificar Titulo");
        abuilder.setView(editTitulo);
        abuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        abuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!editTitulo.getText().toString().isEmpty()){
                    titulo_fondo = editTitulo.getText().toString();
                    s_dialog = Snackbar.make(fab, getResources().getString(R.string.msg_guardando_titulo), Snackbar.LENGTH_INDEFINITE);
                    s_dialog.show();
                    StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.UPDATE_TITULO, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                        Log.d("Response", response);
                            if (!response.isEmpty()) {
                                if (response.equals("0")) {
                                } else {
                                    cargar_lista_fondos();
                                }
                            }
                        }
                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
//                                Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                                    s_dialog.dismiss();
                                    Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("login", login);
                            params.put("pass", pass);
                            params.put("titulo", titulo_fondo);
                            params.put("fondo", fondo_activo);
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/x-www-form-urlencoded");
                            return headers;
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(postRequest, "GUARDA_SENSOR1");

                }else{
                    Snackbar.make(bkg,getResources().getString(R.string.err_titulo_fondo),Snackbar.LENGTH_LONG).show();

                }

            }
        });
        abuilder.show();

    }

    private void agregarMapa() {

        PBM.setVisibility(View.VISIBLE);

        final Spinner centrales = new Spinner(this);
        getCentrales(centrales);
        final EditText editTitulo = new EditText(this);
        editTitulo.setSingleLine();
        editTitulo.setHint("Titulo del Mapa");
        AlertDialog.Builder abuilder = new AlertDialog.Builder(this);
        abuilder.setTitle("Agregar Mapa");
        abuilder.setIcon(R.mipmap.question_icon);
        abuilder.setView(editTitulo);
        abuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PBM.setVisibility(View.INVISIBLE);
                return;
            }
        });
        abuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!editTitulo.getText().toString().isEmpty()){
                    titulo_fondo = editTitulo.getText().toString();
                    AlertDialog.Builder bbuilder = new AlertDialog.Builder(SensorsActivity.this);
                    bbuilder.setTitle("Seleccione la Central");
                    bbuilder.setIcon(R.mipmap.question_icon);
                    bbuilder.setView(centrales);
                    bbuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PBM.setVisibility(View.INVISIBLE);
                            return;
                        }
                    });
                    bbuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            central_activa = (String)centrales.getItemAtPosition(centrales.getSelectedItemPosition());
                            Toast.makeText(SensorsActivity.this,"Central: "+central_activa,Toast.LENGTH_LONG).show();
                            pickImage(SELECT_PICTURE_CHANGE);
                        }
                    });
                    bbuilder.create().show();
                }else{
                    Snackbar.make(bkg,getResources().getString(R.string.err_titulo_fondo),Snackbar.LENGTH_LONG).show();
                    editTitulo.selectAll();
                }

            }
        });
        abuilder.show();

    }

    private void borraNivelSensores() {

        PBM.setVisibility(View.VISIBLE);

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.ELIMINA_NIVEL_SENSORES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                if (Integer.parseInt(response) >= 0) {
                    for (int i = 0; i < Sensors.size(); i++) {
                        RL.removeView(Sensors.elementAt(i));
                    }
                    Sensors.clear();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("sensores", response);
                    editor.commit();
                    //leerSensoresTimer();
                    RL.invalidate();
                    borraNivelMapa();
                    Snackbar.make(PBM, getResources().getString(R.string.msg_sensores_eliminados), Snackbar.LENGTH_LONG).show();
                    PBM.setVisibility(View.INVISIBLE);
                } else {
                    Snackbar.make(PBM, getResources().getString(R.string.err_borrando_sensores), Snackbar.LENGTH_LONG).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        PBM.setVisibility(View.INVISIBLE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("fondo", fondo_activo);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL3");
    }

    private void borraNivelMapa() {
        PBM.setVisibility(View.VISIBLE);

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.ELIMINA_NIVEL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                if (response.equals("1")) {
                    fondo_activo = "";
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("map", "");
                    editor.putString("map_nro", "0");
                    editor.commit();
                    RL.invalidate();
                    cargar_lista_fondos();
                    Snackbar.make(PBM, getResources().getString(R.string.msg_fondo_eliminado), Snackbar.LENGTH_LONG).show();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        PBM.setVisibility(View.INVISIBLE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("id_fondo", fondo_activo);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL4");
    }

    private void pickImage(int to) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), to);
        PBM.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent _data) {
        super.onActivityResult(requestCode, resultCode, _data);
        final Intent data = _data;
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                //Get ImageURi and load with help of picasso
                //Uri selectedImageURI = data.getData();

                CT = new CustomTarget(bkg, PBM, getApplicationContext(), "AlarmSMS", login.trim() + ".jpeg");
                AppController.p
                        .load(data.getData())
                        .into(CT);
                fondo_cambiado = true;
            }
            if (requestCode == SELECT_PICTURE_CHANGE) {
                CT = new CustomTarget(bkg, PBM, getApplicationContext(), "AlarmSMS", login.trim() + ".jpeg");
                AppController.p
                        .load(data.getData())
                        .into(bkg, new Callback() {
                            @Override
                            public void onSuccess() {
                                fondo_cambiado = true;
                                if (bkg.getDrawable() == null) {
                                    Snackbar.make(fab, getResources().getString(R.string.error_fondo), Snackbar.LENGTH_LONG).show();
                                    return;
                                }
                                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constants.UPLOAD_URL, new Response.Listener<NetworkResponse>() {
                                    @Override
                                    public void onResponse(NetworkResponse response) {
                                        String resultResponse = new String(response.data);
//                Log.i("RESPONSE:",resultResponse);
                                        s_dialog.dismiss();
                                        if (resultResponse.contains(login)) {
                                            Snackbar.make(bkg, getResources().getString(R.string.msg_enviado), Snackbar.LENGTH_LONG).show();
                                            cargar_lista_fondos();
//                                            if (listaFondos.getChildCount() > 0)
//                                                listaFondos.setSelection(0);

                                        } else {
                                            Snackbar.make(bkg, getResources().getString(R.string.error_guardando_fondo), Snackbar.LENGTH_LONG).show();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        NetworkResponse networkResponse = error.networkResponse;
                                        String errorMessage = "Unknown error";
                                        if (networkResponse == null) {
                                            if (error.getClass().equals(TimeoutError.class)) {
                                                errorMessage = "Request timeout";
                                            } else if (error.getClass().equals(NoConnectionError.class)) {
                                                errorMessage = "Failed to connect server";
                                            }
                                        } else {
                                            String result = new String(networkResponse.data);
                                            try {
                                                JSONObject response = new JSONObject(result);
                                                String status = response.getString("status");
                                                String message = response.getString("message");

//                        Log.e("Error Status", status);
//                        Log.e("Error Message", message);

                                                if (networkResponse.statusCode == 404) {
                                                    errorMessage = "Resource not found";
                                                } else if (networkResponse.statusCode == 401) {
                                                    errorMessage = message + " Please login again";
                                                } else if (networkResponse.statusCode == 400) {
                                                    errorMessage = message + " Check your inputs";
                                                } else if (networkResponse.statusCode == 500) {
                                                    errorMessage = message + " Something is getting wrong";
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
//                Log.i("Error", errorMessage);
                                        error.printStackTrace();
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<>();
                                        params.put("login", login);
                                        params.put("pass", pass);
                                        params.put("id_fondo", "0");
                                        params.put("titulo", titulo_fondo);
                                        params.put("central",central_activa);
                                        params.put("MAX_FILE_SIZE","10000000");
                                        return params;
                                    }

                                    @Override
                                    protected Map<String, DataPart> getByteData() {
                                        Map<String, DataPart> params = new HashMap<>();
                                        // file name could found file base or direct access from real path
                                        // for now just get bitmap data from ImageView
                                        params.put("userFile", new DataPart("map.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), bkg.getDrawable()), "image/jpeg"));
                                        return params;
                                    }
                                };
                                multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                AppController.getInstance().addToRequestQueue(multipartRequest, "LOGIN");
                            }

                            @Override
                            public void onError() {

                            }
                        });

            }
        }

    }

    private void guardar_fondo() {
        if (fondo_cambiado == false)
            return;
        if (bkg.getDrawable() == null) {
            Snackbar.make(fab, getResources().getString(R.string.error_fondo), Snackbar.LENGTH_LONG).show();
            return;
        }
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constants.UPLOAD_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
//                Log.i("RESPONSE:",resultResponse);
                s_dialog.dismiss();
                if (resultResponse.contains(login)) {
                    Snackbar.make(bkg, getResources().getString(R.string.msg_enviado), Snackbar.LENGTH_LONG).show();
                    cargar_lista_fondos();
                } else {
                    Snackbar.make(bkg, getResources().getString(R.string.error_guardando_fondo), Snackbar.LENGTH_LONG).show();
                }
                fondo_cambiado = false;
                s_dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

//                        Log.e("Error Status", status);
//                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
//                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("id_fondo", fondo_activo);
                params.put("titulo", titulo_fondo);
                params.put("central",central_activa);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("userFile", new DataPart("map.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), bkg.getDrawable()), "image/jpeg"));
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(multipartRequest, "LOGIN1");
    }

    private void guardar_sensores() {
        final boolean[] error = {false};
        if (Sensors.size() == 0) {
            if (Sensors_bin.size() > 0) {
                eliminar_sensores();
            }
            return;
        }
        s_dialog = Snackbar.make(fab, getResources().getString(R.string.msg_guardando_sensores), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();
        for (final MySensor ms : Sensors) {
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.GUARDA_SENSOR_FONDO, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
//                        Log.d("Response", response);
                    if (!response.isEmpty()) {
                        if (response.equals("0")) {
//                                Log.e("Error Guardando Sensor:",response);
                            error[0] = true;
                        } else {
//                                Log.i("Sensor "+String.valueOf(ms.getID())+"Guardado:",response);
                            guarda_sensores_local(Sensors.indexOf(ms));
                        }
                    }
                    s_dialog.dismiss();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                                Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                            s_dialog.dismiss();
                            Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("login", login);
                    params.put("pass", pass);
                    params.put("sensor_id", String.valueOf(ms.getID()));
                    float posx_perc = (ms.getPosX() * 100) / width;
                    params.put("sensor_x", String.valueOf(posx_perc));
                    float posy_perc = (ms.getPosY() * 100) / height;
                    params.put("sensor_y", String.valueOf(posy_perc));
                    params.put("tipo", ms.getTipo());
                    params.put("fondo", fondo_activo);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(postRequest, "GUARDA_SENSOR3");
        }
        if (Sensors_bin.size() > 0) {
            eliminar_sensores();
        }

        if (error[0] == false) {
            Snackbar.make(bkg, getResources().getString(R.string.msg_sensores_guardados), Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(bkg, getResources().getString(R.string.error_guardando_sensores), Snackbar.LENGTH_LONG).show();
        }
    }

    private void guarda_sensores_local(int x) {
        Sensors_temp.add(Sensors.elementAt(x));
        if (Sensors.size() == Sensors_temp.size()) {
            JSONArray _ja = new JSONArray();
            for (int i = 0; i < Sensors.size(); i++) {
                Map<String, String> _map = new HashMap<>();
                MySensor _ms = Sensors.elementAt(i);
                _map.put("id_sensor", String.valueOf(i));
                _map.put("posx_sensor", String.valueOf((_ms.getPosX() * 100) / width));
                _map.put("posy_sensor", String.valueOf((_ms.getPosY() * 100) / height));
                _map.put("nombre_id_sensor", String.valueOf(_ms.getID()));
                _map.put("tipo_sensor", _ms.getTipo());
                _map.put("id_user_sensor", id_user);
                JSONObject _jo = new JSONObject(_map);
                _ja.put(_jo);
            }
//            Log.d("JSON_LOCAL", _ja.toString());
            Sensors_temp.clear();
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("sensores", _ja.toString());
            editor.commit();

        }

    }

    private void leerSensores(boolean refresh) {
//        s_dialog = Snackbar.make(fab,getResources().getString(R.string.msg_cargando_sensores),Snackbar.LENGTH_INDEFINITE);
//        s_dialog.show();
        if (settings.getString("sensores", "").equals("") || refresh) {
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.GET_SENSORS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (!response.isEmpty()) {
//                    Log.i("GET_SENSORS:",response);
                        for (int i = 0; i < Sensors.size(); i++) {
                            RL.removeView(Sensors.elementAt(i));
                        }
                        Sensors.clear();
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("sensores", response);
                        editor.commit();
                        leerSensoresTimer();
//                        s_dialog.dismiss();
                        RL.invalidate();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                            s_dialog.dismiss();
                            Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("login", login);
                    params.put("pass", pass);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(postRequest, "GET_SENSORS4");
        } else {
            leerSensoresTimer();
//            s_dialog.dismiss();
        }

    }

    private void leerSensoresXfondo(final String id_fondo) {
//        s_dialog = Snackbar.make(fab,getResources().getString(R.string.msg_cargando_sensores),Snackbar.LENGTH_INDEFINITE);
//        s_dialog.show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.GET_SENSORSxFONDO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.isEmpty()) {
//                    Log.i("GET_SENSORS:",response);
                    for (int i = 0; i < Sensors.size(); i++) {
                        RL.removeView(Sensors.elementAt(i));
                    }
                    Sensors.clear();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("sensores", response);
                    editor.commit();
                    leerSensoresTimer();
//                        s_dialog.dismiss();
                    RL.invalidate();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("id_fondo", id_fondo);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_SENSORS5");
        listaFondos.setEnabled(true);
        btn_next_map.setEnabled(true);
        btn_prev_map.setEnabled(true);
    }

    private void carga_sensores_local() {
        timerTaskS = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            String json_sensors = settings.getString("sensores", "");
//                            Log.i("JSON",json_sensors.toString());
                            JSONArray json_array = new JSONArray(json_sensors);
                            Sensors.clear();
                            for (int i = 0; i < json_array.length(); i++) {
                                JSONObject jobj = new JSONObject(json_array.getString(i));
                                String _id_user = jobj.getString("id_user_sensor");
                                if (!_id_user.equals(id_user)) {
                                    s_dialog.dismiss();
                                    Snackbar.make(bkg, "Los Sensores almacenados no pertenecen a este usuario, debe actualizar la data", Snackbar.LENGTH_INDEFINITE).show();
                                    break;
                                }
                                int posx = jobj.getInt("posx_sensor");
                                int posy = jobj.getInt("posy_sensor");
                                int ID = jobj.getInt("nombre_id_sensor");
                                String tipo = jobj.getString("tipo_sensor");
//                            loadSensor(tipo,posx,posy,ID);
                                edit_id_sensor.setText(String.valueOf(ID));
                                MySensor ms = addSensor(tipo,true);
                                int _x = (posx * width) / 100;
                                int _y = (posy * height) / 100;
                                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) ms.getLayoutParams();
                                _x = lParams.leftMargin + _x;
                                _y = lParams.topMargin + _y;
                                ms.setPosX(_x);
                                ms.setPosY(_y);
                                ms.setLeft(_x);
                                ms.setTop(_y);
                                lParams.leftMargin = _x;
                                lParams.topMargin = _y;
                                ms.setLayoutParams(lParams);
                                ms.postInvalidate();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        RL.invalidate();
                    }
                });
            }
        };
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private Target picassoImageTarget(Context context, final String imageDir, final String imageName) {
//        Log.d("picassoImageTarget", " picassoImageTarget");
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE); // path to /data/data/yourapp/app_imageDir
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
//                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {
                }
            }

        };
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
        final int sel_pos = position;
        if (check == 0) {
            check++;
            return;
        }
        listaFondos.setEnabled(false);
        btn_next_map.setEnabled(false);
        btn_prev_map.setEnabled(false);
        String item = (String) parent.getItemAtPosition(position);
        final String fondo_id = getFondoId(item);
//        Toast.makeText(SensorsActivity.this, fondo_id, Toast.LENGTH_SHORT).show();
        PBM.setVisibility(View.VISIBLE);
        s_dialog = Snackbar.make(fab, getResources().getString(R.string.msg_cargando_fondo), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getfondo_method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                if (!response.isEmpty() && !response.equals("0")) {
                    try {
                        System.out.println(response);
                        JSONObject json_obj = new JSONObject(response);
                        map = json_obj.getString("map");
                        central_activa = json_obj.getString("central");
                        Toast.makeText(SensorsActivity.this,"Central: "+central_activa,Toast.LENGTH_LONG).show();
                        ((TextView)findViewById(R.id.label_central)).setText(central_activa);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("map", map);
                        editor.putString("map_nro", String.valueOf(sel_pos));
                        editor.putString("central",central_activa);
                        editor.commit();
                        CT = new CustomTarget(bkg, PBM, getApplicationContext(), "AlarmSMS", login.trim() + ".jpeg");
                        AppController.p
                                .load(map)
                                .into(CT);
                        s_dialog.dismiss();
                        leerSensoresXfondo(fondo_id);
                        cargar_comandos();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(fab, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                        PBM.setVisibility(View.INVISIBLE);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("id_fondo", fondo_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL6");
    }

    private String getFondoId(String item) {
        titulo_fondo = item.substring(0, item.indexOf("(")-1);
        fondo_activo = item.substring(item.indexOf("(") + 1, item.indexOf(")"));
        return fondo_activo;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public void getCentrales(final Spinner modelos) {

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getCentrales, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    List<String> list = new ArrayList<String>();
                    JSONArray json_array= new JSONArray(response);
                    for (int i=0;i<json_array.length();i++) {
                        JSONObject json_obj = new JSONObject(json_array.getString(i));
                        list.add(json_obj.getString("modelo_central"));
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SensorsActivity.this,
                            android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    modelos.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    modelos.setAdapter(dataAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder adb = new AlertDialog.Builder(SensorsActivity.this);
                adb.setTitle("Mensaje");
                adb.setIcon(R.mipmap.error_icon);
                adb.setMessage("Hubo un error al intentar cargar los modelos de las centrales");
                adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog a = adb.create();
                a.show();
            }
        });
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_CENTRALES");
    }
}

