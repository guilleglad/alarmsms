package com.guilleglad.alarmsms.util;

import android.content.Context;
import android.os.Vibrator;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * Created by Guillermo García on 09-10-2016.
 */
public class SMSSender {

    public SMSSender(){

    }
    public void enviarSMS(String Comando, String number, String pass, String alias, Context context) {

        StringBuilder _sb = new StringBuilder();
        //_sb.append(alias).append(":");
        _sb.append(pass).append(Comando);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, _sb.toString(), null, null);
            Vibrator v = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
            v.vibrate(500);
        }catch(Exception e){
            Toast.makeText(context,
                    "No se pudo enviar el SMS, intente mas tarde.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
