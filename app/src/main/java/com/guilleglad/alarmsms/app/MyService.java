package com.guilleglad.alarmsms.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.guilleglad.alarmsms.MainActivity;
import com.guilleglad.alarmsms.R;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;

/**
 * Created by Guillermo García on 24-06-2016.
 */
public class MyService extends Service {
    private SharedPreferences settings;
    public static final String PREFS_NAME = "AlarmSMS_data";
    private String sms_number;

    @Override
    public void onDestroy() {
//        Toast.makeText(getBaseContext(),"Servicio Apagado",Toast.LENGTH_LONG).show();
//        super.onDestroy();
    }

    @Override
    public void onCreate() {
//        Intent notificationIntent = new Intent(getBaseContext(), MainActivity.class);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0,
//                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Notification notification = new NotificationCompat.Builder(getBaseContext())
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle("AlarmSMS")
//                .setContentText("Servicio de Alarma")
//                .setContentIntent(pendingIntent).build();
//
//        startForeground(1337, notification);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        settings = getSharedPreferences(PREFS_NAME, 0);
        sms_number = settings.getString("sms_number","");
//        Log.i("SMSRADAR:","Iniciando SMSRADAR...");
//        Toast.makeText(getBaseContext(),"Servicio Iniciado",Toast.LENGTH_LONG).show();
        SmsRadar.stopSmsRadarService(getApplicationContext());
        SmsRadar.initializeSmsRadarService(getApplicationContext(), new SmsListener() {

            @Override
            public void onSmsSent(Sms sms) {

            }

            @Override
            public void onSmsReceived(Sms sms) {
                String msg = sms.getMsg();
                String log = "SMS RECIBIDO "+sms.getMsg()+" de "+sms.getAddress();
//                Log.d("SMSRadar",log);
//                Toast.makeText(getApplicationContext(),"SMS RECIBIDO",Toast.LENGTH_LONG).show();
                sms_number = settings.getString("sms_number","");
//                Toast.makeText(getApplicationContext(),"Número Recibido: "+sms.getAddress(),Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),"Número a Recibir: "+sms_number,Toast.LENGTH_LONG).show();
                if(msg.contains("ID:") && sms.getAddress().equals(sms_number))
                {

                    Intent I = new Intent(getBaseContext(),MainActivity.class);
                    I.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    I.putExtra("msg",msg);
                    getBaseContext().startActivity(I);
                }

            }

        });
        return START_STICKY;
    }

}
