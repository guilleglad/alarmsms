package com.guilleglad.alarmsms.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Guillermo García on 24-06-2016.
 */
public class MySensor extends ImageView {
    private int posX;
    private int posY;
    private int ID;
    private String Tipo;

    public MySensor(Context C, int posX, int posY, int ID, String tipo){
        super(C);
        setPosX(posX);
        setPosY(posY);
        setID(ID);
        setTipo(tipo);
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getID() {
        return ID;
    }

    public MySensor(Context context) {
        super(context);
    }

    public MySensor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MySensor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        Paint p =new Paint();
//        p.setColor(Color.BLACK);
//        p.setStyle(Paint.Style.FILL);
//        p.setTextSize(getWidth()/3);
//        canvas.drawText(String.valueOf(this.getID()),getWidth()/3,getHeight()/2,p);
//    }
}
