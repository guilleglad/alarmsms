package com.guilleglad.alarmsms.misc;

/**
 * Created by Guillermo García on 24-03-2016.
 */
public class Constants {
    public static final long SPLASH_SCREEN_DELAY = 3000;
    //public static final String base_url = "http://192.168.1.147/alarmsms/";
    public static final String base_url = "http://52.41.114.72/alarmsms2/";
    public static final String url = base_url+"Rest/";
    public static final String UPLOAD_URL = base_url+"upload/do_upload";
    public static final String UPLOAD_URL_NEW = base_url+"upload/do_upload2";
    public static final String getmap_method = "getMapa";
    public static final String getfondo_method = "getFondo";
    public static final String getmaps_method = "getallFondos";
    public static final String MAP_FILE = "userFile";
    public static final String GUARDA_SENSOR = "setSensor";
    public static final String GUARDA_SENSOR_FONDO = "setSensorFondo";
    public static final String GET_SENSORS = "getallSensors";
    public static final String GET_SENSORSxFONDO = "getallSensorsFondo";
    public static final String ELIMINA_SENSOR = "delSensor" ;
    public static final String ELIMINA_SENSOR_FONDO = "delSensorFondo";
    public static final String ELIMINA_NIVEL_SENSORES = "delallSensorsFondo";
    public static final String ELIMINA_NIVEL = "delFondo";
    public static final String UPDATE_TITULO = "setTitulo";
    public static final String getComandos = "getComandos";
    public static String login_method = "login";


    public static String get_id_user = "getClientId";
    public static String getCentrales = "getCentralmodelos";
    public static String setRegistro = "setRegistro";
    public static String restorepass = "restorepass";
    public static String setMapaCentral = "setmapaycentral";
}
