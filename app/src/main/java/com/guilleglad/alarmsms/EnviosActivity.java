package com.guilleglad.alarmsms;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.guilleglad.alarmsms.app.AppController;
import com.guilleglad.alarmsms.misc.Constants;
import com.guilleglad.alarmsms.util.CustomTarget;
import com.guilleglad.alarmsms.util.SMSSender;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.asm.tree.analysis.Frame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnviosActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String PREFS_NAME = "AlarmSMS_data";
    private Snackbar s_dialog;
    private TableLayout TL;
    private Spinner listaFondos,listaCentrales;
    private SharedPreferences settings;
    private String titulo_fondo;
    private String fondo_activo;
    private String login,pass,central;
    private EditText edit_numero,edit_pass;
    private FrameLayout layoutProgreso;
    private ImageView p25,p50,p75,p100;
    private Button btn_activar,btn_desactivar,btn_activarparcial,btn_estado,btn_activarsirena,btn_desactivarsirena;
    private Button btn_activarpgm,btn_desactivarpgm,btn_consultarsaldo,btn_verhistorial;
    private Button btn_activarpgmrf1,btn_desactivarpgmrf1,btn_activarpgmrf2,btn_desactivarpgmrf2;
    private TextView msg_enviando,msg_enviado;
    private String activar,activarParcial,desactivar,estado,activarsirena,desactivarsirena,activarpgm ,desactivarpgm ,consultarsaldo ,verhistorial;
    private String activarpgmrf1,desactivarpgmrf1,activarpgmrf2,desactivarpgmrf2;
    private ImageView btn_back;
    private SMSSender mySender;
    private ImageView btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envios);
        recibir_valores();
        init_objects();
        cargar_lista_fondos();

    }

    private void cargar_comandos() {
//        s_dialog = Snackbar.make(TL, getResources().getString(R.string.msg_cargando_comandos), Snackbar.LENGTH_INDEFINITE);
//        s_dialog.show();

        StringRequest comandosRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getComandos, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                System.out.println(response);
                if (!response.isEmpty() && !response.equals("0")) {
                    try {

                        JSONArray jarr = new JSONArray(response);
                        JSONObject jobj = new JSONObject(jarr.getString(0));
                        activar = jobj.getString("sms_activar_central");
                        if(activar.isEmpty()) {
                            btn_activar.setEnabled(false);
                            btn_activar.setBackgroundColor(Color.GRAY);
                            btn_activar.setTag("");
                        }
                        else
                            btn_activar.setTag(activar);
                        activarParcial = jobj.getString("sms_activarparcial_central");
                        if(activarParcial.isEmpty()){
                            btn_activarparcial.setEnabled(false);
                            btn_activarparcial.setBackgroundColor(Color.GRAY);
                            btn_activarparcial.setTag("");
                        }
                        else
                            btn_activarparcial.setTag(activarParcial);
                        desactivar = jobj.getString("sms_desactivar_central");
                        if(desactivar.isEmpty()) {
                            btn_desactivar.setEnabled(false);
                            btn_desactivar.setBackgroundColor(Color.GRAY);
                        }
                        else
                            btn_desactivar.setTag(desactivar);
                        estado = jobj.getString("sms_estado_central");
                        if(estado.isEmpty()) {
                            btn_estado.setEnabled(false);
                            btn_estado.setBackgroundColor(Color.GRAY);
                            btn_estado.setTag("");
                        }
                        else
                            btn_estado.setTag(estado);
                        activarsirena = jobj.getString("sms_activarsirena_central");
                        if(activarsirena.isEmpty()) {
                            btn_activarsirena.setEnabled(false);
                            btn_activarsirena.setBackgroundColor(Color.GRAY);
                            btn_activarsirena.setTag("");
                        }
                        else
                            btn_activarsirena.setTag(activarsirena);
                        desactivarsirena = jobj.getString("sms_desactivarsirena_central");
                        if(desactivarsirena.isEmpty()){
                            btn_desactivarsirena.setEnabled(false);
                            btn_desactivarsirena.setBackgroundColor(Color.GRAY);
                            btn_desactivarsirena.setTag("");
                        }
                        else
                            btn_desactivarsirena.setTag(desactivarsirena);
                        activarpgm = jobj.getString("sms_activarpgm_central");
                        if(activarpgm.isEmpty()){
                            btn_activarpgm.setEnabled(false);
                            btn_activarpgm.setBackgroundColor(Color.GRAY);
                            btn_activarpgm.setTag("");
                        }
                        else
                            btn_activarpgm.setTag(activarpgm);
                        desactivarpgm = jobj.getString("sms_desactivarpgm_central");
                        if(desactivarpgm.isEmpty()){
                            btn_desactivarpgm.setEnabled(false);
                            btn_desactivarpgm.setBackgroundColor(Color.GRAY);
                            btn_desactivarpgm.setTag("");
                        }
                        else
                            btn_desactivarpgm.setTag(desactivarpgm);
                        consultarsaldo = jobj.getString("sms_consultarsaldo_central");
                        if(consultarsaldo.isEmpty()){
                            btn_consultarsaldo.setEnabled(false);
                            btn_consultarsaldo.setBackgroundColor(Color.GRAY);
                            btn_consultarsaldo.setTag("");
                        }
                        else
                            btn_consultarsaldo.setTag(consultarsaldo);
                        verhistorial = jobj.getString("sms_verhistorial_central");
                        if(verhistorial.isEmpty()){
                            btn_verhistorial.setEnabled(false);
                            btn_verhistorial.setBackgroundColor(Color.GRAY);
                            btn_verhistorial.setTag("");
                        }
                        else
                            btn_verhistorial.setTag(verhistorial);
                        activarpgmrf1 = jobj.getString("sms_activarpgmrf1_central");
                        if(activarpgmrf1.isEmpty()){
                            btn_activarpgmrf1.setEnabled(false);
                            btn_activarpgmrf1.setBackgroundColor(Color.GRAY);
                            btn_activarpgmrf1.setTag("");
                        }
                        else
                            btn_activarpgmrf1.setTag(activarpgmrf1);
                        desactivarpgmrf1 = jobj.getString("sms_desactivarpgmrf1_central");
                        if(desactivarpgmrf1.isEmpty()){
                            btn_desactivarpgmrf1.setEnabled(false);
                            btn_desactivarpgmrf1.setBackgroundColor(Color.GRAY);
                            btn_desactivarpgmrf1.setTag("");
                        }
                        else
                            btn_desactivarpgmrf1.setTag(desactivarpgmrf1);
                        activarpgmrf2 = jobj.getString("sms_activarpgmrf2_central");
                        if(activarpgmrf2.isEmpty()){
                            btn_activarpgmrf2.setEnabled(false);
                            btn_activarpgmrf2.setBackgroundColor(Color.GRAY);
                            btn_activarpgmrf2.setTag("");
                        }
                        else
                            btn_activarpgmrf2.setTag(activarpgmrf2);
                        desactivarpgmrf2 = jobj.getString("sms_desactivarpgmrf2_central");
                        if(desactivarpgmrf2.isEmpty()){
                            btn_desactivarpgmrf2.setEnabled(false);
                            btn_desactivarpgmrf2.setBackgroundColor(Color.GRAY);
                            btn_desactivarpgmrf2.setTag("");
                        }
                        else
                            btn_desactivarpgmrf2.setTag(desactivarpgmrf2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//                    s_dialog.dismiss();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(TL, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                central = settings.getString("central","");
                params.put("central",central);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(comandosRequest, "GET_COMANDOS");

    }


    private void recibir_valores() {
        Bundle extras = getIntent().getExtras();
        if(!extras.isEmpty()){
            login = extras.getString("login","");
            pass = extras.getString("pass","");
            central = extras.getString("central","");
        }
    }

    private void init_objects() {
        settings = getSharedPreferences(PREFS_NAME, 0);
        mySender = new SMSSender();
        TL = (TableLayout)findViewById(R.id.botonera);
        listaFondos = (Spinner)findViewById(R.id.listaFondos);
        listaFondos.setEnabled(false);
        listaCentrales = (Spinner)findViewById(R.id.listaCentrales);
        getCentrales(listaCentrales);
        edit_numero = (EditText)findViewById(R.id.edit_numero);
        edit_numero.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    SharedPreferences.Editor edit = settings.edit();
                    edit.putString("numero_sms_enviar",((EditText)v).getText().toString());
                    edit.commit();
                }
            }
        });
        edit_numero.setText(settings.getString("numero_sms_enviar",""));
        edit_pass = (EditText)findViewById(R.id.edit_pass);
        edit_pass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    SharedPreferences.Editor edit = settings.edit();
                    edit.putString("pass_sms",((EditText)v).getText().toString());
                    edit.commit();
                }
            }
        });
        edit_pass.setText(settings.getString("pass_sms",""));
        layoutProgreso = (FrameLayout)findViewById(R.id.layout_progreso);
        p25 = (ImageView)findViewById(R.id.p25);
        p50 = (ImageView)findViewById(R.id.p50);
        p75 = (ImageView)findViewById(R.id.p75);
        p100 = (ImageView)findViewById(R.id.p100);
        btn_activar = (Button)findViewById(R.id.btn_activar);
        btn_activar.setOnClickListener(this);
        btn_activarparcial = (Button)findViewById(R.id.btn_activarparcial);
        btn_activarparcial.setOnClickListener(this);
        btn_desactivar = (Button)findViewById(R.id.btn_desactivar);
        btn_desactivar.setOnClickListener(this);
        btn_estado = (Button)findViewById(R.id.btn_estado);
        btn_estado.setOnClickListener(this);
        btn_activarsirena = (Button)findViewById(R.id.btn_activarsirena);
        btn_activarsirena.setOnClickListener(this);
        btn_desactivarsirena = (Button)findViewById(R.id.btn_desactivarsirena);
        btn_desactivarsirena.setOnClickListener(this);
        btn_activarpgm = (Button)findViewById(R.id.btn_activarpgm);
        btn_activarpgm.setOnClickListener(this);
        btn_desactivarpgm = (Button)findViewById(R.id.btn_desactivarpgm);
        btn_desactivarpgm.setOnClickListener(this);
        btn_consultarsaldo = (Button)findViewById(R.id.btn_consultarsaldo);
        btn_consultarsaldo.setOnClickListener(this);
        btn_verhistorial = (Button)findViewById(R.id.btn_verhistorial);
        btn_verhistorial.setOnClickListener(this);
        btn_activarpgmrf1 = (Button)findViewById(R.id.btn_activarpgmrf1);
        btn_activarpgmrf1.setOnClickListener(this);
        btn_desactivarpgmrf1 = (Button)findViewById(R.id.btn_desactivarpgmrf1);
        btn_desactivarpgmrf1.setOnClickListener(this);
        btn_activarpgmrf2 = (Button)findViewById(R.id.btn_activarpgmrf2);
        btn_activarpgmrf2.setOnClickListener(this);
        btn_desactivarpgmrf2 = (Button)findViewById(R.id.btn_desactivarpgmrf2);
        btn_desactivarpgmrf2.setOnClickListener(this);
        msg_enviando = (TextView)findViewById(R.id.msg_enviando);
        msg_enviado = (TextView)findViewById(R.id.msg_enviado);
        btn_back = (ImageView)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator.animate(v).tada().duration(400).onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        finish();
                    }
                }).start();
            }
        });
        btn_save = (ImageView)findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator.animate(v).tada().duration(400).start();
                SharedPreferences.Editor edit = settings.edit();
                edit.putString("numero_sms_enviar", edit_numero.getText().toString());
                edit.putString("pass_sms", edit_pass.getText().toString());
                edit.putString("sms_number", edit_numero.getText().toString());
                edit.commit();
                Snackbar.make(v,getResources().getString(R.string.msg_guardado),Snackbar.LENGTH_LONG).show();
                guardar_mapaycentral();

            }
        });
        cargar_comandos();
    }

    private void guardar_mapaycentral() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.setMapaCentral, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                SharedPreferences.Editor editor = settings.edit();
                central = listaCentrales.getSelectedItem().toString();
                editor.putString("central",central);
                editor.commit();
                Toast.makeText(EnviosActivity.this, "Central Modificada", Toast.LENGTH_LONG).show();
                finish();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(TL, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                params.put("map",getFondoId(listaFondos.getSelectedItem().toString()));
                params.put("central",listaCentrales.getSelectedItem().toString());
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL");
    }


    private void animarProgreso(){
        long intervalos = 1200;
        long in = 1000;
        long out = 400;
        ViewAnimator
                .animate(layoutProgreso)
                .alpha(0,1)
                .duration(in)
                .accelerate()
                .thenAnimate(p25)
                .alpha(0,1)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p50)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p25)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p75)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p50)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p100)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(p75)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .thenAnimate(p100)
                .alpha(1,0)
                .duration(intervalos)
                .andAnimate(msg_enviado)
                .alpha(0,1)
                .duration(intervalos)
                .andAnimate(msg_enviando)
                .descelerate()
                .thenAnimate(p100)
                .alpha(1,0)
                .duration(out)
                .andAnimate(layoutProgreso)
                .alpha(1,0)
                .duration(out)
                .andAnimate(msg_enviado)
                .alpha(1,0)
                .duration(intervalos)
                .descelerate()
                .start();

    }

    private void cargar_lista_fondos(){
        s_dialog = Snackbar.make(TL, getResources().getString(R.string.msg_cargando_fondos), Snackbar.LENGTH_INDEFINITE);
        s_dialog.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getmaps_method, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("Response", response);
                final String resp;
                if (!response.isEmpty() && !response.equals("0")) {
                    try {
                        List<String> list = new ArrayList<String>();
                        JSONArray jarr = new JSONArray(response);
                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject jobj = new JSONObject(jarr.getString(i));
                            String id = jobj.getString("id_fondo");
                            String nombre = jobj.getString("titulo_fondo");
                            list.add(nombre + " (" + id + ")");
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        listaFondos.setPrompt("Mapas Disponibles");
                        listaFondos.setAdapter(dataAdapter);
                        String map_nro = settings.getString("map_nro", "");
                        if (!map_nro.isEmpty()) {
                            listaFondos.setSelection(Integer.parseInt(map_nro));
                        }
                        if (list.size() > 0) {
                            String item;
                            if (map_nro.isEmpty()) {
                                item = listaFondos.getItemAtPosition(0).toString();
                            } else {
                                if (Integer.parseInt(map_nro) >= list.size())
                                    map_nro = "0";
                                item = listaFondos.getItemAtPosition(Integer.parseInt(map_nro)).toString();
                            }
                            if (!item.isEmpty())
                                fondo_activo = getFondoId(item);
                        }
                        System.out.println(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    s_dialog.dismiss();
                }if(response.equals("[]")){
                    Snackbar.make(listaFondos, getResources().getString(R.string.msg_no_fondos), Snackbar.LENGTH_INDEFINITE).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                        s_dialog.dismiss();
                        Snackbar.make(TL, getResources().getString(R.string.volleyerror), Snackbar.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("pass", pass);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(postRequest, "GET_MAP_URL");

    }

    private String getFondoId(String item) {
        titulo_fondo = item.substring(0, item.indexOf("("));
        fondo_activo = item.substring(item.indexOf("(") + 1, item.indexOf(")"));
        return fondo_activo;
    }

    @Override
    public void onClick(final View v) {
        ViewAnimator
                .animate(v)
                .tada()
                .duration(500)
                .accelerate()
                .start().onStop(new AnimationListener.Stop() {
            @Override
            public void onStop() {
                if(edit_numero.getText().toString().isEmpty()) {
                    Snackbar.make(edit_numero, getResources().getString(R.string.error_number_empty), Snackbar.LENGTH_LONG).show();
                    edit_numero.requestFocus();
                }else{
                    if(edit_pass.getText().toString().isEmpty()) {
                        Snackbar.make(edit_pass, getResources().getString(R.string.error_empty_pass), Snackbar.LENGTH_LONG).show();
                        edit_pass.requestFocus();
                    }else {
                        animarProgreso();
                        String fondo = listaFondos.getSelectedItem().toString();
                        fondo = fondo.substring(0,fondo.indexOf("(")-1);
                        mySender.enviarSMS((String) v.getTag(),
                                edit_numero.getText().toString(),
                                edit_pass.getText().toString(),
                                fondo,
                                getApplicationContext());
                    }
                }
            }
        });
    }
    public void getCentrales(final Spinner modelos) {
        ((ProgressBar)findViewById(R.id.progress_c)).setVisibility(View.VISIBLE);
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.getCentrales, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    List<String> list = new ArrayList<String>();
                    JSONArray json_array= new JSONArray(response);
                    for (int i=0;i<json_array.length();i++) {
                        JSONObject json_obj = new JSONObject(json_array.getString(i));
                        list.add(json_obj.getString("modelo_central"));
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    modelos.setAdapter(dataAdapter);
                    if (!central.equals(null)) {
                        int position = dataAdapter.getPosition(central);
                        modelos.setSelection(position);
                    }
                    ((ProgressBar)findViewById(R.id.progress_c)).setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(EnviosActivity.this);
                adb.setTitle("Mensaje");
                adb.setIcon(R.mipmap.error_icon);
                adb.setMessage("Hubo un error al intentar cargar los modelos de las centrales");
                adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                android.app.AlertDialog a = adb.create();
                a.show();
            }
        });
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "GET_CENTRALES");
    }
}
