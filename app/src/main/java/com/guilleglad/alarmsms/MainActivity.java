package com.guilleglad.alarmsms;

import android.Manifest;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.guilleglad.alarmsms.app.AppController;
import com.guilleglad.alarmsms.app.MyService;
import com.guilleglad.alarmsms.net.MyJsonObjectRequest;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.guilleglad.alarmsms.misc.Constants;


public class MainActivity extends AppCompatActivity{
    Button btn_login;
    EditText edit_login;
    EditText edit_pass;
    EditText edit_sms_number;
    ImageView img_logo;
    private String login;
    private String pass;
    public static final String PREFS_NAME = "AlarmSMS_data";
    int logged_in;
    private SharedPreferences settings;
    private String ID="";
    private String id_user="";
    private MyService my_service;
    private String sms_number="";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private String permission;
    private ProgressBar PB1;
    Button btn_register;
    private Button btn_forget_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.READ_SMS,
                Manifest.permission.SEND_SMS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};

            if(!checkPermissions(this, PERMISSIONS)){
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        }

//    private void checkPermissions() {
//        if(Build.VERSION.SDK_INT >= 23){
//            int hasReadSmsPermission = checkSelfPermission(Manifest.permission.READ_SMS);
//            if (hasReadSmsPermission != PackageManager.PERMISSION_GRANTED) {
//                requestPermissions(new String[] {Manifest.permission.READ_SMS,Manifest.permission.SEND_SMS},
//                        REQUEST_CODE_ASK_PERMISSIONS);
//                return;
//            }
//            process_extradata();
//            init();
//        }else{
//            process_extradata();
//            init();
//        }
//
//    }
    private boolean checkPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        process_extradata();
        init();
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    process_extradata();
                    init();
                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_sms_permiso), Toast.LENGTH_SHORT)
                            .show();
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }


    }

    private void init(){
//        service_init();
        my_service_init();
        obj_init();
        events_init();
        check_login();
    }

    private void my_service_init() {
//        Notification note=new Notification(android.R.drawable.ic_dialog_info,
//                "AlarmSMS Iniciado",
//                System.currentTimeMillis());
        Intent _s = new Intent(getApplicationContext(),MyService.class);
        startService(_s);

    }

    @Override
    protected void onResume() {
        super.onResume();
        init();

    }

    private void check_login() {
        //REVISA SI YA INICIO SESION
        settings = getSharedPreferences(PREFS_NAME, 0);
        logged_in = settings.getInt("logged_in", 0);
        login = settings.getString("login","");
        pass = settings.getString("pass","");
        id_user = settings.getString("id_user","");
        sms_number = settings.getString("sms_number","");
        edit_sms_number.setText(settings.getString("sms_number",""));

        if(logged_in == 1 || logged_in == 2)
        {
            goto_sensors(String.valueOf(logged_in));
        }
    }

    private void events_init() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(v)
                        .tada()
                        .duration(400)
                        .descelerate()
                        .start();
                PB1.setVisibility(View.VISIBLE);
                if(edit_login.getText().toString().isEmpty() || edit_pass.getText().toString().isEmpty() /*|| edit_sms_number.getText().toString().isEmpty()*/)
                {
                    Snackbar.make(btn_login,getResources().getString(R.string.faltan_campos),Snackbar.LENGTH_LONG).show();
                    return;
                }else{
                    login = edit_login.getText().toString();
                    pass = edit_pass.getText().toString();
                    sms_number = edit_sms_number.getText().toString();
                    if(!sms_number.equals(""))
                    {
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("sms_number",sms_number);
                        editor.commit();

                    }
                    StringRequest postRequest = new StringRequest(Request.Method.POST,Constants.url+Constants.login_method, new Response.Listener<String>(){

                        @Override
                        public void onResponse(String response) {
//                            Log.d("Response", response);
                            PB1.setVisibility(View.INVISIBLE);
                            if(!response.isEmpty()){
                                SharedPreferences.Editor editor = settings.edit();
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                switch(response.charAt(0)){
                                    case '0':
                                        Snackbar.make(edit_pass,getResources().getString(R.string.error_login),Snackbar.LENGTH_LONG).show();
                                        imm.hideSoftInputFromWindow(edit_pass.getWindowToken(), 0);
                                        break;
                                    case '1':
                                        Snackbar.make(edit_pass,getResources().getString(R.string.login_1),Snackbar.LENGTH_LONG).show();
                                        editor.putString("login",login);
                                        editor.putString("pass",pass);
                                        editor.putString("sms_number",sms_number);
                                        editor.putInt("logged_in", Integer.parseInt(String.valueOf(response.charAt(0))));
                                        id_user = response.substring(1);
                                        editor.putString("id_user",id_user);
                                        editor.commit();
                                        goto_sensors(response);
                                        break;
                                    case '2':
                                        Snackbar.make(btn_login,getResources().getString(R.string.login_2),Snackbar.LENGTH_LONG).show();
                                        editor.putString("login",login);
                                        editor.putString("pass",pass);
                                        editor.putString("sms_number",sms_number);
                                        editor.putInt("logged_in", Integer.parseInt(String.valueOf(response.charAt(0))));
                                        id_user = response.substring(1);
                                        editor.putString("id_user",id_user);
                                        editor.commit();
                                        goto_sensors(response);
                                        break;
                                    default:
                                        Snackbar.make(edit_pass,getResources().getString(R.string.error_login),Snackbar.LENGTH_LONG).show();
                                        imm.hideSoftInputFromWindow(edit_pass.getWindowToken(), 0);
                                        break;
                                }

                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Log.d("Error.Response", "VOLLEYERROR: "+error.getStackTrace());
                            Snackbar.make(btn_login,getResources().getString(R.string.volleyerror),Snackbar.LENGTH_LONG).show();
                        }
                    }
                    ){
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("login",login);
                            params.put("pass",pass);
                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders()
                        {
                            Map<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/x-www-form-urlencoded");
                            return headers;
                        }
                    };
                    AppController.getInstance().addToRequestQueue(postRequest,"LOGIN");

                }
            }
        });
        edit_pass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    btn_login.callOnClick();
                    return true;
                }
                return false;
            }
        });
        btn_forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(v)
                        .tada()
                        .duration(400)
                        .descelerate()
                        .onStop(new AnimationListener.Stop() {
                            @Override
                            public void onStop() {
                                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                                adb.setTitle("Restablecimiento de Contraseña");
                                adb.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        recover_password();
                                    }
                                });
                                adb.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                adb.setMessage("Recibirá un correo con la información para Restablecer su contraseña, ¿Desea Continuar?");
                                AlertDialog ad = adb.create();
                                ad.show();
                            }
                        })
                        .start();

            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAnimator
                        .animate(v)
                        .tada()
                        .duration(400)
                        .descelerate()
                        .onStop(new AnimationListener.Stop() {
                            @Override
                            public void onStop() {
                                Intent I = new Intent(MainActivity.this,RegisterActivity.class);
                                startActivity(I);
                            }
                        })
                        .start();

            }
        });
    }
    public static String MD5_Hash(String s) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(s.getBytes(),0,s.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }
    private void recover_password() {
        if(!edit_login.getText().toString().equals("")) {
            StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.restorepass+"/"+MD5_Hash(edit_login.getText().toString()), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    if(Integer.parseInt(response) > 0)
                    {
                        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                        adb.setTitle("Restablecimiento de Contraseña");
                        adb.setIcon(R.mipmap.ok_icon);
                        adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        adb.setMessage("En breve recibirá un correo electrónico con las instrucciones necesarias para restablecer su contraseña. ");
                        AlertDialog ad = adb.create();
                        ad.show();
                    }else{
                        if(Integer.parseInt(response)==-3){
                            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                            adb.setTitle("Error");
                            adb.setIcon(R.mipmap.error_icon);
                            adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            adb.setMessage("No tiene un correo electrónico asignado a su cuenta. Contacte con un administrador.");
                            AlertDialog ad = adb.create();
                            ad.show();
                        }else {
                            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                            adb.setTitle("Error");
                            adb.setIcon(R.mipmap.error_icon);
                            adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            adb.setMessage("Hubo problemas en la comunicación, intente mas tarde. ");
                            AlertDialog ad = adb.create();
                            ad.show();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(postRequest, "GET_CENTRALES");

        }else{
            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
            adb.setTitle("Error");
            adb.setIcon(R.mipmap.error_icon);
            adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            adb.setMessage("Debe ingresar su Nombre de Usuario para restablecer la contraseña. ");
            AlertDialog ad = adb.create();
            ad.show();
        }
    }

    private void goto_sensors(String response) {
        Intent I = new Intent(getApplicationContext(),SensorsActivity.class);
        I.putExtra("tipo_login",Character.toString(response.charAt(0)));
        I.putExtra("login",login);
        I.putExtra("pass",pass);
        I.putExtra("ID",ID);
        I.putExtra("id_user",id_user);
        startActivity(I);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        process_extradata();
    }

    private void process_extradata() {
        Intent I = getIntent();
        if(I != null)
        {
            ID = I.getStringExtra("msg");
        }
    }

    private void obj_init() {
        btn_login = (Button)findViewById(R.id.btn_login);
        edit_login = (EditText)findViewById(R.id.edit_login);
        edit_pass = (EditText)findViewById(R.id.edit_pass);
        img_logo = (ImageView)findViewById(R.id.img_logo);
        edit_sms_number = (EditText)findViewById(R.id.edit_sms_number);
        PB1 = (ProgressBar)findViewById(R.id.progressBar1);
        btn_register = (Button)findViewById(R.id.btn_register);
        btn_forget_pass = (Button)findViewById(R.id.btn_forget_password);

    }

//    private void service_init() {
//        //Inicializa el Servicio
//        Intent _s = new Intent(getApplicationContext(),MyService.class);
//        getApplicationContext().startService(_s);
//        SmsRadar.initializeSmsRadarService(this, new SmsListener() {
//
//            @Override
//            public void onSmsSent(Sms sms) {
//
//            }
//
//            @Override
//            public void onSmsReceived(Sms sms) {
//                String msg = sms.getMsg();
//                String log = "SMS RECIBIDO "+sms.getMsg()+" de "+sms.getAddress();
////                Log.d("SMSRadar",log);
//                Toast.makeText(getApplicationContext(),"SMS RECIBIDO",Toast.LENGTH_LONG).show();
//                sms_number = settings.getString("sms_number","");
//                Toast.makeText(getApplicationContext(),"Número Recibido: "+sms.getAddress(),Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),"Número a Recibir: "+sms_number,Toast.LENGTH_LONG).show();
//                if(msg.contains("ID:") && sms.getAddress().equals(sms_number))
//                {
//                    Intent I = new Intent(getBaseContext(),MainActivity.class);
//                    I.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    I.putExtra("msg",msg);
//                    startActivity(I);
//                }
//
//            }
//
//        });
//    }
}

