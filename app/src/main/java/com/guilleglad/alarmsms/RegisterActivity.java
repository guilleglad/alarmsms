package com.guilleglad.alarmsms;

import android.content.DialogInterface;
import android.net.nsd.NsdManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.guilleglad.alarmsms.app.AppController;
import com.guilleglad.alarmsms.misc.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {


    ProgressBar progressbar1;
    private Button btn_register;
    private EditText input_login,input_pass,input_tlf,input_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_save);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.INVISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Registro de Usuario");

        progressbar1 = (ProgressBar)findViewById(R.id.progress_centrales);

        //progressbar1.setVisibility(View.VISIBLE);
        input_login = (EditText)findViewById(R.id.input_login);
        input_pass = (EditText)findViewById(R.id.input_pass);
        input_tlf = (EditText)findViewById(R.id.input_telefono);
        input_email = (EditText)findViewById(R.id.input_email);
        btn_register = (Button)findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(input_login.getText().toString()) && !isEmpty(input_pass.getText().toString()) && !isEmpty(input_tlf.getText().toString()) && !isEmpty(input_email.getText().toString())) {
                    registerUser();
                } else {
                    if (isEmpty(input_login.getText().toString())) {
                        ((TextInputLayout)findViewById(R.id.textinput_nombre)).setError("Campo Requerido");
                    } else {
                        ((TextInputLayout)findViewById(R.id.textinput_nombre)).setError(null);
                    }
                    if (isEmpty(input_pass.getText().toString())) {
                        ((TextInputLayout)findViewById(R.id.textinput_password)).setError("Campo Requerido");
                    } else {
                        ((TextInputLayout)findViewById(R.id.textinput_password)).setError(null);
                    }
                    if (isEmpty(input_tlf.getText().toString())) {
                        ((TextInputLayout)findViewById(R.id.textinput_telefono)).setError("Campo Requerido");
                    } else {
                        ((TextInputLayout)findViewById(R.id.textinput_telefono)).setError(null);
                    }
                    if (isEmpty(input_email.getText().toString())) {
                        ((TextInputLayout)findViewById(R.id.textinput_email)).setError("Campo Requerido");
                    } else {
                        ((TextInputLayout)findViewById(R.id.textinput_email)).setError(null);
                    }
                }
            }
        });


    }
    public boolean isEmpty(String string) {
        if (string.equals("")) {
            return true;
        } else {
            return false;
        }
    }
    private void registerUser() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constants.url + Constants.setRegistro, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                if(response.equals("1")){
                    AlertDialog.Builder adb = new AlertDialog.Builder(RegisterActivity.this);
                    adb.setTitle("¡Muy Bien!");
                    adb.setIcon(R.mipmap.ok_icon);
                    adb.setMessage("Usuario Almacenado, por favor siga las instrucciones que llegarán a su cuenta de correo electrónico para activar su cuenta. Gracias");
                    adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    AlertDialog a = adb.create();
                    a.show();
                }else if(response.equals("2")) {
                    AlertDialog.Builder adb = new AlertDialog.Builder(RegisterActivity.this);
                    adb.setTitle("Error");
                    adb.setIcon(R.mipmap.error_icon);
                    adb.setMessage("Error, el usuario ya está registrado, en caso de no recordar la contraseña por favor restablezcala en la pantalla principal");
                    adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    AlertDialog a = adb.create();
                    a.show();
                }else{
                    AlertDialog.Builder adb = new AlertDialog.Builder(RegisterActivity.this);
                    adb.setTitle("Mensaje");
                    adb.setIcon(R.mipmap.notif_icon);
                    adb.setMessage("Hubo un error al intentar registrarse, intente mas tarde nuevamente o comuniquese con servicio técnico");
                    adb.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    AlertDialog a = adb.create();
                    a.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", input_login.getText().toString());
                params.put("pass", input_pass.getText().toString());
                params.put("telefono",input_pass.getText().toString());
                params.put("email",input_email.getText().toString());
                Log.i("PARAMS", String.valueOf(params));
                return params;
            }

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest, "REGISTER_USER");
    }


}
